import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.*;


public class View extends JFrame {
	
	private JFrame frame;
	private JPanel panelIn;
	private JPanel panelOut;
	private JPanel panel;
	private JTextField textIn;
	private JLabel label;
	
	private JTextArea textOut;
	
	private JComboBox comboBox;
	private JButton run;
	
	public View(){
		frame = new JFrame("Nested Loop");
		panelIn = new JPanel();
		panelOut = new JPanel();
		panel = new JPanel();
		textIn = new JTextField(30);
		label = new JLabel("input :");
		textOut = new JTextArea("");
		comboBox =new JComboBox();
		run = new JButton();
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(400,400);
		
		panelIn.setLayout(new GridLayout(3,1));
		panel.add(label);
		panel.add(textIn);
		
		comboBox.addItem("Ẻ��� 1");
		comboBox.addItem("Ẻ��� 2");
		comboBox.addItem("Ẻ��� 3");
		comboBox.addItem("Ẻ��� 4");
		
		run = new JButton("run");
		
		panelIn.add(panel);
		panelIn.add(comboBox);
		panelIn.add(run);
		panelOut.setLayout(new GridLayout(1,1));
		panelOut.add(textOut);
		frame.setLayout(new GridLayout(2,1));
		frame.add(panelIn);
		frame.add(panelOut);
		
		frame.setVisible(true);
	}
	
	
	public JTextField getTextIn(){
		return textIn;
	}
	public JTextArea getTextOut(){
		return textOut;
	}
	public JComboBox getComboBox(){
		return comboBox;
	}
	public JButton getButton(){
		return run;
	}

}
