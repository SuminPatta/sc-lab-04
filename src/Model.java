import java.util.ArrayList;

public class Model {
	
	private String ans;
	private String a;
	
	public String nestedLoop(int loop,int num){
		ans= "";
		if(loop == 0){
			for(int i = 1;i <= num ; i++){
				a = "";
				for(int j = 1; j <= num ; j++){
					a += "*";
				}
				ans = ans+a;
				if (i != num){
					ans = ans +"\n";
				}
			}
		}
		else if(loop == 1){
			for(int i = 1;i <= num ; i++){
				a = "";
				for(int j = 1; j <= i ; j++){
					a += "*";
				}
				ans = ans+a;
				if (i != num){
					ans = ans +"\n";
				}
			}
		}
		else if(loop == 2){
			for(int i = 1;i <= num ; i++){
				a = "";
				for(int j = 1; j <= num ; j++){
					if( j%2 == 0){
						a+="*";
					}
					else{
						a+="-";
					}
				}
				ans = ans+a;
				if (i != num){
					ans = ans +"\n";
				}
			}
		}
		else if(loop == 3){
			for(int i = 1;i <= num ; i++){
				a = "";
				for(int j = 1; j <= num ; j++){
					if((i+j)%2 == 0){
						a +="*";
					}
					else {
						a+=" ";
					}
				}
				ans = ans+a;
				if (i != num){
					ans = ans +"\n";
				}
			}
		}
		
		return ans;
	}

}
