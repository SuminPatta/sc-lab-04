import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;


public class Controller {
	
	private View frame;
	private Model model;
	private AddInterestListener listener;
	
	public static void main(String[] args){
		new Controller();
	}
	
	public Controller(){
		frame = new View();
		model = new Model();
		listener = new AddInterestListener();
		frame.getButton().addActionListener(listener);
	}
	
	
	class AddInterestListener implements ActionListener
	{
		public void actionPerformed(ActionEvent event)
		{
			int num = Integer.parseInt(frame.getTextIn().getText());
			int loop = frame.getComboBox().getSelectedIndex();
			frame.getTextOut().setText(model.nestedLoop(loop, num));
		}            
	}

}
